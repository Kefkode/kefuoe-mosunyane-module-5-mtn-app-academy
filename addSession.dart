import 'dart:developer';

import 'package:flutter/material.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addSession() {
      final name = nameController.text;
      final subject = subjectController.text;
      final location = locationController.text;

    
      final ref = FirebaseFirestore.instance.collection("sessions").doc();

      return ref
          .set({"Name": name, "Subject": subject, "Location": location})
          .then((value)=> log("Collection added!!"))
          .catchError((onError)=> log(onError));
    }

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: nameController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                  hintText: "Enter Name")),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: subjectController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                  hintText: "Enter Subject")),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular((20)),
                    ),
                    hintText: "Enter Location"))),
        ElevatedButton(
          onPressed: () {
            _addSession{};
          }, child: Text("Add Session"))
      ],
    );
  }
}
